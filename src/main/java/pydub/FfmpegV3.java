package pydub;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.bytedeco.javacpp.Loader;

import com.google.common.collect.Lists;

import cn.hutool.core.io.FileUtil;

/**
 * 提取字幕
 * @author Administrator
 *FFmpeg可以从嵌入式字幕轨道中“读取”或“extract提取”字幕。例如，如果您运行:'ffmpeg -i <my_file>' 可以看到类似如下:
 */
public class FfmpegV3 {
	
	static String likeStr = "subtitle";
	
	public static void main(String[] args) throws InterruptedException, IOException {
		 //String ffmpeg = Loader.load(org.bytedeco.ffmpeg.ffmpeg.class);
	     //ProcessBuilder pb = new ProcessBuilder(ffmpeg, "-i", "F:/face/vido/1.avi");
	     //ProcessBuilder pb = new ProcessBuilder(ffmpeg, "-codecs");
	     //后台执行
	//	 InputStream in = pb.inheritIO().start().getInputStream();
		 /*InputStream in = pb.start().getInputStream();
		 String str = IOUtils.toString(in, "utf-8");
		 System.out.println("===========");
		 String[] lines = str.split("\n");
		 for(String p : lines){
			 if(p != null && p.contains(likeStr)){
				 System.out.println(p.replaceAll("\n", "")); 
			 } 
		 } 
		 System.out.println("===========");*/
		 
		 String  s = "Duration: 00:00:04.47, start: 0.050113, bitrate: 32 kb/s".toLowerCase();
		 Pattern rec2 = Pattern.compile("duration: (\\d+):(\\d+):(\\d+).(\\d)");
		 Matcher match =rec2.matcher(s);  
        if(match.find()){
        	String index = match.group();
        	index = index.substring(10,index.length());
        	String[] dur = index.split(":");
        	String[] dur1 = dur[2].split("\\."); 
        	Float duration =  Float.valueOf(dur[0]) * 60 * 60 + Float.valueOf(dur[1]) * 60 +  Float.valueOf(dur1[0]) +  Float.valueOf(dur1[1]) / 10; 
            System.out.println(duration);
        } 
	}
}
