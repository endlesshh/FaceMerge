package pydub;

import java.io.File;
import java.io.IOException;
import java.nio.Buffer;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber.Exception;

import ai.djl.modality.cv.ImageFactory;

public class FFmpegFrameRead {
	public static void main(String[] args) throws UnsupportedAudioFileException, IOException {
		 File vf = new File("E:\\code\\git_work\\ttskit-main\\ttskit\\resource\\audio\\biaobei-biaobei-009502.mp3");
	     /*FFmpegFrameGrabber grabberI = FFmpegFrameGrabber.createDefault(vf);
	     grabberI.start();
	     System.out.println("总时长:"+grabberI.getLengthInTime()/1000/60);
	     System.out.println("总音频长:"+grabberI.getLengthInAudioFrames());
	     System.out.println("总视频长:"+grabberI.getLengthInVideoFrames());
	     System.out.println("总贞长:"+grabberI.getLengthInFrames());
	     int audios = grabberI.getLengthInAudioFrames() >= Integer.MAX_VALUE ? 0 : grabberI.getLengthInAudioFrames();
	     int vidoes = grabberI.getLengthInVideoFrames() >= Integer.MAX_VALUE ? 0 : grabberI.getLengthInVideoFrames();
	     Frame frame = null;
	     int frame_number =  audios;
	     for (int i = 0; i < frame_number; i++) { 
	    	 frame = grabberI.grabSamples();
	    	 Buffer[] bf = frame.samples;
	    	 for(int j=0;j<bf.length;j++){
	    		 bf[j].flip();
	    		 
	    	 } 
	     }*/
	      
	     AudioInputStream in= AudioSystem.getAudioInputStream(vf);
	     AudioInputStream din = null;
	     AudioFormat baseFormat = in.getFormat();
	     AudioFormat decodedFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 
	                                                 baseFormat.getSampleRate(),
	                                                 16,
	                                                 baseFormat.getChannels(),
	                                                 baseFormat.getChannels() * 2,
	                                                 baseFormat.getSampleRate(),
	                                                 false);
	     din = AudioSystem.getAudioInputStream(decodedFormat, in);
	     while (true){
	    	    int currentByte = din.read();
	    	    if (currentByte == -1) break;
	    	    System.out.println(currentByte);
	    	    // Handling code
	    	}
	}
}
