package pydub;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.bytedeco.javacpp.Loader;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

import ai.djl.Device;
import ai.djl.modality.cv.Image;
import ai.djl.modality.cv.util.NDImageUtils;
import ai.djl.ndarray.NDArray;
import ai.djl.ndarray.NDManager;
import ai.djl.ndarray.types.DataType;
import ai.djl.ndarray.types.Shape;

/**
 * 提取字幕 
 */
public class FfmpegYes {
	
	static String likeStr = "subtitle";
	public static int samplerate = 0;
	public static int channels = 0;
	public static float duration = 0.0f;
	
	public static Queue<byte[]> audio_open() throws InterruptedException, IOException {
		 
		String ffmpeg = Loader.load(org.bytedeco.ffmpeg.ffmpeg.class); 
		ProcessBuilder pb1 = new ProcessBuilder(ffmpeg, "-i",
				 "E:\\code\\git_work\\ttskit-main\\ttskit\\resource\\audio\\biaobei-biaobei-009502.mp3"); 
		 
		BufferedReader br = new BufferedReader(new InputStreamReader(pb1.start().getErrorStream(),"UTF-8"));
		List<String> out_parts = Lists.newArrayList();
		String ch = "";
		while((ch=br.readLine())!= null){ 
			 out_parts.add(ch.toLowerCase()); 
             _parse_info(Joiner.on("").join(out_parts)); 
		}
		  
		/* System.out.println(samplerate);
		 System.out.println(channels);
		 System.out.println(duration);  */  
		 ProcessBuilder pb = new ProcessBuilder(ffmpeg, "-i",
				 "E:\\code\\git_work\\ttskit-main\\ttskit\\resource\\audio\\biaobei-biaobei-009502.mp3",
				 "-f","s16le","-"); 
		 InputStream input = pb.start().getInputStream();
		 byte[] data = new byte[1024]; 
		 int rc = 0;
		 Queue<byte[]> queue = new LinkedList<byte[]>();
         while ((rc = input.read(data)) > 0) {
        	 queue.add(data);
         }	
	     input.close(); 
	     return queue;
	}
	
	public static List<NDArray> __audioread_load(String path,float duration,int offset,DataType dtype) throws Exception{
		List<NDArray> y = Lists.newArrayList();
		Queue<byte[]> queue = audio_open();
		
		int sr_native = samplerate;
		int n_channels = channels;
		int s_start = Math.round(sr_native * offset) * n_channels;
        int s_end = 0;
		if (duration == 0.0){
            s_end = Integer.MAX_VALUE;
        }else{
            s_end = s_start + (Math.round(sr_native * duration) * n_channels);
        }
		int  n = 0 ;
		
		NDManager manager = NDManager.newBaseManager(Device.cpu());
	     for(byte[] que: queue){
	    	 NDArray frame = buf_to_float(que,manager);
	    	 int n_prev = n;
	    	 n = n + (int) frame.size();
	         if( n < s_start){
	                //# offset is after the current frame
	               // # keep reading
	                continue;
	         }
             if( s_end < n_prev){
                // we're off the end.  stop reading
                break;
             }
             if( s_end < n){
                 // the end is in this frame.  crop.
            	 frame = frame.get(":"+(s_end - n_prev)); 
             }
             if( n_prev <= s_start && s_start <= n){
                 // beginning is in this frame 
                 frame = frame.get((s_start - n_prev)+":"); 
             }
             y.add(frame); 
	     }
	      
		 System.out.println("===========");
		return null;
	}
	
	
	
	private static NDArray buf_to_float(byte[] frame, NDManager manager){
		int n_bytes=2;
		float scale = (float)1.0 / Float.valueOf(1 << ((8 * n_bytes) - 1));
		return manager.create(frame).mul(scale).toType(DataType.FLOAT32,false);	 
	}
    public static void _parse_info(String s){
        /* Given relevant data from the ffmpeg output, set audio
        parameter fields on this object.*/
    	Pattern re = Pattern.compile("(\\d+) hz");
        // Sample rate.
    	Matcher match =re.matcher(s);  
        if(match.find()){
            samplerate = Integer.valueOf(match.group(1));
        }else{
            samplerate = 0;
        }
        // Channel count.
        Pattern rec = Pattern.compile("hz, ([^,]+),");
        match =rec.matcher(s);
        if(match.find()){
            String mode = match.group(1);
            if (mode == "stereo"){
                channels = 2;
            }else{
            	Pattern rec1 = Pattern.compile("(\\d+)\\.?(\\d)?");
            	Matcher cmatch =rec1.matcher(mode);  
                if(cmatch.find()){
                    channels = Stream.of(cmatch.group().split("\\.")).mapToInt(t -> Integer.valueOf(t)).sum();
                }else{
                    channels = 1;
                }
            }
        }else{
            channels = 0;
        }
        // Duration.
        Pattern rec2 = Pattern.compile("duration: (\\d+):(\\d+):(\\d+)\\.(\\d)");
        match =rec2.matcher(s);  
        if(match.find()){
        	String index = match.group();
        	index = index.substring(10,index.length());
        	String[] dur = index.split(":");
        	String[] dur1 = dur[2].split("\\."); 
        	duration =  Float.valueOf(dur[0]) * 60 * 60 + Float.valueOf(dur[1]) * 60 +  Float.valueOf(dur1[0]) +  Float.valueOf(dur1[1]) / 10; 
             
        } 
    }
}
