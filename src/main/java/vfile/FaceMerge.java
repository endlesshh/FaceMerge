package vfile;
import org.springframework.util.ResourceUtils;
import util.OpenCVFaceSwap;

import java.io.FileNotFoundException;

/**
 * @ClassName: FaceMerge
 * @description: 启动类
 * @author: endless
 * @create: 2020年5月29日14:00:46
 **/
public class FaceMerge {
    public static void main(String[] args) throws FileNotFoundException {
        String path1 = ResourceUtils.getFile("classpath:img/3m.jpg").getAbsolutePath();
        String path2 = ResourceUtils.getFile("classpath:img/2m.jpg").getAbsolutePath();

        /*String path1 = "F:\\face\\1\\2m.jpg";
        String path2 = "F:\\face\\1\\9.jpg"; */
        String savePath = "F:\\face\\";//图片存放位置
        // 参数说明
        // type ： opencv和baidu 两种获取人脸标记的位置点
        // jingxi : true 使用全部点位进行分割，false使用外部轮廓的点位进行融合
        OpenCVFaceSwap.faceMerge(path1,path2,savePath,"opencv",true);
    }
}
