package vfile.translate;

import ai.djl.modality.cv.Image;
import ai.djl.modality.cv.util.NDImageUtils;
import ai.djl.ndarray.NDArray;
import ai.djl.ndarray.NDList;
import ai.djl.ndarray.index.NDIndex;
import ai.djl.ndarray.types.DataType;
import ai.djl.ndarray.types.Shape;
import ai.djl.translate.Batchifier;
import ai.djl.translate.Translator;
import ai.djl.translate.TranslatorContext;
import ai.djl.util.Pair;

import java.util.HashMap;
import java.util.Map;

public class NPtKTranslator implements Translator<NDArray, Map> {

    public NPtKTranslator() {

    }
    /** {@inheritDoc} */
    @Override
    public NDList processInput(TranslatorContext ctx, NDArray input) {
        /*NDArray img = input.toNDArray(ctx.getNDManager());
        img = NDImageUtils.resize(img, 256, 256);
        img = img.div(255);
        img = img.transpose(2, 0, 1);
        img = img.toType(DataType.FLOAT32,false);*/
        /*int h = input.getHeight();
        int w = input.getWidth();*/
        //int[] hw = scale(h, w, 960);
        // img = NDImageUtils.resize(img, hw[1], hw[0]);
        
        //img = NDImageUtils.toTensor(img);
       
        /*img =
                NDImageUtils.normalize(
                        img,
                        new float[]{0.485f, 0.456f, 0.406f},
                        new float[]{0.229f, 0.224f, 0.225f});*/

       //.transpose(0, 3, 1, 2);
      /*  System.out.println("img.toDebugString(100000, 1000, 1000, 1000)");
        System.out.println(img.toDebugString(1000000000, 1000, 1000, 1000));*/
        //img = img.expandDims(0);
        return new NDList(input.get(0));
    }

    /** {@inheritDoc} */
    @Override
    public Map<String,NDArray> processOutput(TranslatorContext ctx, NDList list) {
        //默认行为是 情空
        Map a = new HashMap();
        NDArray jacobian = list.get(0);
        jacobian.detach();
        NDArray value = list.get(1);
        value.detach();
        a.put(list.get(0).getName(),jacobian);
        a.put(list.get(1).getName(),value);
        return a;
    }

    /** {@inheritDoc} */
    @Override
    public Batchifier getBatchifier() {
        return Batchifier.STACK;
    }

}
